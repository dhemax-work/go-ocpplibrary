package cs

import (
	"errors"
	"fmt"
	"net/http"
	"net/http/httputil"
	"strings"

	"github.com/eduhenke/go-ocpp"
	"github.com/eduhenke/go-ocpp/internal/log"
	"github.com/eduhenke/go-ocpp/internal/service"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpreq"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpresp"
	"github.com/eduhenke/go-ocpp/ws"
)

// ChargePointMessageHandler handles the OCPP messages coming from the charger
type ChargePointMessageHandler func(cprequest cpreq.ChargePointRequest, cpID string) (cpresp.ChargePointResponse, error)

//CentralSystem ...
type CentralSystem interface {
	// Run the central system on the given port
	// and handles each incoming ChargepointRequest
	Run(port string, cphandler ChargePointMessageHandler) error

	// GetServiceOf a chargepoint to enable
	// communication with the chargepoint
	//
	// the url parameter is *NOT* used if
	// the link between the CentralSystem
	// and Chargepoint is via Websocket
	GetServiceOf(cpID string, version ocpp.Version, url string) (service.ChargePoint, error)
}

type centralSystem struct {
	conns map[string]*ws.Conn
}

//New ...
func New() CentralSystem {
	return &centralSystem{
		conns: make(map[string]*ws.Conn, 0),
	}
}

//ConnectedMachines ...
var ConnectedMachines []string
var machineLogs = make(map[string][]string)

//ReturnLogs ...
func ReturnLogs() map[string][]string {
	return machineLogs
}

func ReturnAllLogs() []string {
	return machineLogs["_all_"]
}

//ContainLogs ...
func ContainLogs(machineID string) bool {
	for i := range machineLogs {
		if i == machineID {
			return true
		}
	}
	return false
}

//AddLog ...
func AddLog(machineID string, text string) {

	if IsMachineConnect(machineID) {
		if !ContainLogs(machineID) {
			machineLogs[machineID] = []string{text}
		} else {
			machineLogs[machineID] = append(machineLogs[machineID], text)
		}
	} else {
		machineLogs["_all_"] = append(machineLogs[machineID], text)
	}
	fmt.Printf("\n[MACHINE LOG] %v", text)
}

//IsMachineConnect ...
func IsMachineConnect(machineConn string) bool {
	for i := range ConnectedMachines {
		if ConnectedMachines[i] == machineConn {
			return true
		}
	}
	return false
}

//ListMachines ...
func ListMachines() string {
	var ListString string = ""
	for i := range ConnectedMachines {
		ListString += "Machine ID #" + fmt.Sprintf("%v", i+1) + " [" + fmt.Sprintf("%v", ConnectedMachines[i]) + "]\n"
	}
	return ListString
}

//HaveEmpty ...
func HaveEmpty() bool {

	for _, element := range ConnectedMachines {
		if element == " " {
			return true
		}
	}

	return false
}

//RemoveMachine ...
func RemoveMachine(machineID string) {
	for i, v := range ConnectedMachines {
		if v == machineID {
			ConnectedMachines = append(ConnectedMachines[:i], ConnectedMachines[i+1:]...)
		}
	}
	fmt.Printf("\n===[The machine ID %v has been disconnected]===\n", machineID)
}

func (csys *centralSystem) Run(port string, cphandler ChargePointMessageHandler) error {
	log.Debug("== [CENTRAL SYSTEM SERVER ONLINE] ==\n")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if ws.IsWebSocketUpgrade(r) {
			csys.handleWebsocket(w, r, cphandler)
		}
	})

	log.Debug("Central system running on port: [%s]", port)
	return http.ListenAndServe(port, nil)
}

func (csys *centralSystem) handleWebsocket(w http.ResponseWriter, r *http.Request, cphandler ChargePointMessageHandler) {
	log.Debug("Current WS connections map: %v", csys.conns)
	cpID := strings.TrimPrefix(r.URL.Path, "/")

	if cpID == "" {
		log.Error("cpID is empty")
		return
	}

	if IsMachineConnect(cpID) {
		log.Error("\n===[MACHINE ALREADY CONNECTED: %s]===\n", cpID)
		return
	}

	log.Debug("\n===[SE HA CONECTADO: %s]===\n", cpID)
	ConnectedMachines = append(ConnectedMachines, cpID)

	rawReq, _ := httputil.DumpRequest(r, true)
	log.Debug("Raw WS request: %s", string(rawReq))

	conn, err := ws.Handshake(w, r, []ocpp.Version{ocpp.V16})
	if err != nil {
		log.Error("Couldn't handshake request %w", err)
		return
	}
	csys.conns[cpID] = conn
	log.Debug("Connected with %s", cpID)
	AddLog(cpID, "Connected to Central System...")
	go func() {
		for {
			err := conn.ReadMessage()
			if err != nil {
				if !ws.IsNormalCloseError(err) {
					log.Error("On receiving a message: %w", err)
				}
				_ = conn.Close()
				RemoveMachine(cpID)
				log.Debug("Closed connection of: %s", cpID)
				delete(csys.conns, cpID)
				break
			}
		}
	}()
	for {
		req := <-conn.Requests()
		cprequest, ok := req.Request.(cpreq.ChargePointRequest)
		if !ok {
			log.Error(cpreq.ErrorNotChargePointRequest.Error())
		}
		cpresponse, err := cphandler(cprequest, cpID)
		err = conn.SendResponse(req.MessageID, cpresponse, err)
		if err != nil {
			log.Error(err.Error())
		}
	}
}

func (csys *centralSystem) GetServiceOf(cpID string, version ocpp.Version, url string) (service.ChargePoint, error) {
	if version == ocpp.V15 {
		return service.NewChargePointSOAP(url, nil), nil
	}
	if version == ocpp.V16 {
		conn := csys.conns[cpID]
		if conn == nil { // TODO: or conn closed
			return nil, errors.New("no connection to this charge point")
		}
		return service.NewChargePointJSON(conn), nil
	}
	return nil, errors.New("charge point has no configured OCPP version(1.5/1.6)")
}
