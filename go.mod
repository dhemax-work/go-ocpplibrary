module github.com/eduhenke/go-ocpp

require (
	github.com/google/uuid v1.1.0
	github.com/gorilla/websocket v1.4.1
	github.com/rcrowley/goagain v0.0.0-20140424170347-f2f192b5d1a9
	github.com/rs/cors v1.7.0
	github.com/urfave/cli v1.22.2
)

go 1.13
