package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/eduhenke/go-ocpp"
	"github.com/eduhenke/go-ocpp/cs"
	"github.com/eduhenke/go-ocpp/messages/v1x/cpreq"

	"github.com/eduhenke/go-ocpp/messages/v1x/cpresp"
	"github.com/eduhenke/go-ocpp/messages/v1x/csreq"
	"github.com/eduhenke/go-ocpp/messages/v1x/csresp"
	"github.com/rs/cors"
)

/**
	CODIGOS:
	0: Servidor offline
	1: Maquina no operativa
	200: Operación exitosa
	201: Operación rechazada
	300: Hubo un error de parametros
	301: Hubo un error interno
**/

// Variables
// Lista de maquinas conectadas
var machinesStatus []string


// Central de maquinas
var csysVar cs.CentralSystem

//var cpIDVar = ""
var requestChar cpreq.ChargePointRequest

//MachineStatusMessage ...
var MachineStatusMessage = "Offline"
var TheIdTag = ""
var ResponseCode = 0
var cpVersion = ocpp.V16

//WebServerOnline ...
var WebServerOnline = false

//HTTPServer ...
func HTTPServer() {
	if !WebServerOnline {
		fmt.Printf("== [SERVIDOR WEB LEVANTADO] ==\n")
		mux := http.NewServeMux()

		mux.HandleFunc("/api/v1/getmachinelog", GetMahineLogHTTP)

		mux.HandleFunc("/api/v1/getmachines", GetMachinesHTPP)
		mux.HandleFunc("/api/v1/getstatus", GetStatusHTTP)
		mux.HandleFunc("/api/v1/getconfig", GetConfigurationHTTP)
		mux.HandleFunc("/api/v1/changeconfig", ChangeConfigurationHTTP)
		mux.HandleFunc("/api/v1/reset", ResetMachineHTTP)
		mux.HandleFunc("/api/v1/clearcache", ClearCacheHTTP)
		mux.HandleFunc("/api/v1/unlockconnector", UnlockConnectorHTTP)

		mux.HandleFunc("/stoptransaction", RemoteStopHTTP)
		mux.HandleFunc("/getlocallist", GetLocalListHTTP)
		handler := cors.AllowAll().Handler(mux)

		http.ListenAndServe(":8081", handler)
		WebServerOnline = true
	}
}



func GetMahineLogHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var response Response

	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {

			machineID := r.FormValue("machineID")

			if cs.IsMachineConnect(machineID) {
				if len(machineID) < 1{
					response = Response{
						Code:         201,
						Message:      "List of all logs",
						JSONResponse: cs.ReturnLogs(),
					}
				}else{
					response = Response{
						Code:         200,
						Message:      "Machine Logs",
						JSONResponse: cs.ReturnLogs()[machineID],
					}
				}
			} else {
				response = Response{
					Code:         200,
					Message:      "List of all logs",
					JSONResponse: cs.ReturnAllLogs(),
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}
	json.NewEncoder(w).Encode(response)
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	(*w).Header().Set("Content-Type", "application/json")
}

func SetMachineStatus() {
	go func() {
		for {
			time.Sleep(13 * time.Second)
			fmt.Printf("\n ===== [STATUS DE MAQUINAS] =====\n\n")
			if len(cs.ConnectedMachines) > 0 {
				if !cs.HaveEmpty() {
					fmt.Printf("\n== [MAQUINAS CONECTADAS]==\n\t%v\n\n", cs.ListMachines())
				}
			} else {
				fmt.Printf("\nLOG: No hay maquinas conectadas\n")
			}
			fmt.Printf("====================================\n\n")
		}
	}()
}

//Response ...
type Response struct {
	Code         int         `json:"code"`
	Message      string      `json:"message"`
	JSONResponse interface{} `json:"response"`
}

// OCPPSystem ...
func OCPPSystem() {
	//Notas debug de la libreria OCPP
	ocpp.SetDebugLogger(log.New(os.Stdout, "DEBUG: ", log.Ltime))
	ocpp.SetErrorLogger(log.New(os.Stderr, "ERROR: ", log.Ltime))

	//Abrimos una nueva instancia de la clase CS (Central System)
	csysVar = cs.New()

	//Abrimos el servidor en puerto 8090
	go csysVar.Run(":8090", func(req cpreq.ChargePointRequest, cpID string) (cpresp.ChargePointResponse, error) {
		//Confirmamos que iniciamos servidor
		fmt.Printf("INICIO: [Solicitud desde %s]\n", cpID)


		//Realizamos un switch para las posibles respuestas de cliente
		switch req := req.(type) {

		case *cpreq.BootNotification: //Al iniciar, el Punto de Cargo envia información al Central System
			return &cpresp.BootNotification{
				/*
					Status:
					- Accepted 	= Charge point is accepted by Central System.
					- Pending 	= Central System is not yet ready to accept the Charge Point. Central System may send messages to retrieve information or prepare the Charge Point
					- Rejected 	= Charge point is not accepted by Central System.
				*/
				Status:      "Accepted",
				CurrentTime: time.Now(),
				//Interval: Tiempo en segundos para cada latido
				Interval: 25,
			}, nil

		case *cpreq.MeterValues:
			connectorID := req.ConnectorId
			transactionID := req.TransactionId
			text := fmt.Sprintf("Connector: %v TranID: %v \n", connectorID, transactionID)
			fmt.Printf(text)
			cs.AddLog(cpID, text)
			return &cpresp.MeterValues{}, nil
		case *cpreq.Heartbeat:
			fmt.Printf("\n\n == [LATIDO: %s]==\n\n", cpID)

			if !cs.IsMachineConnect(cpID) {
				cs.ConnectedMachines = append(cs.ConnectedMachines, cpID)
			}
			cs.AddLog(cpID, "HeartBeat <3")
			return &cpresp.Heartbeat{CurrentTime: time.Now()}, nil

		case *cpreq.StatusNotification:
			if req.Status == "Available" {
				fmt.Printf("[INFO] El punto de carga esta disponible \n")
			} else if req.Status == "Preparing" {
				fmt.Printf("[INFO] El punto de carga esta preparado \n")
			} else if req.Status == "Charging" {
				fmt.Printf("[INFO] El punto de carga esta cargando \n")
			} else {
				fmt.Printf("[ERROR] El punto de carga no esta disponible \n")
			}
			MachineStatusMessage = req.Status
			cs.AddLog(cpID, req.Status)
			return &cpresp.StatusNotification{}, nil
		case *cpreq.FirmwareStatusNotification:
			//message := req.FirmwareStatusNotification
			//fmt.Printf("Message: %v", message)
			return nil, nil
		case *cpreq.Authorize:

			t := time.Now().Local().Add(time.Minute * 2)
			fmt.Printf("[INFO] Tiempo %s\n", t)

			if req.IdTag != "" {
				fmt.Printf("[INFO] Tarjeta aceptada...\n")
				status := &cpresp.Authorize{IdTagInfo: &cpresp.IdTagInfo{ExpiryDate: &t, ParentIdTag: req.IdTag, Status: "Accepted"}}
				fmt.Printf("[INFO STATUS] %v\n", status)
				cs.AddLog(cpID, fmt.Sprintf("%v", req))
				AsyncTask(csysVar, cpID, req.IdTag)
				return status, nil
			}

			fmt.Printf("[INFO] Tarjeta NO aceptada...\n")

			return &cpresp.Authorize{IdTagInfo: &cpresp.IdTagInfo{ExpiryDate: &t, ParentIdTag: req.IdTag, Status: "Blocked"}}, nil
		default:
			fmt.Printf("[ERROR]: Accion no soportada: %s\n", req.Action())

			return nil, errors.New("la respuesta no es soportada")
		}
	})
}

/* METODOS API */

func GetConfiguration(cpID string, key []string) Response {
	fmt.Printf("\n===[GETTING CONFIG (%v)]===\n", cpID)
	var respo Response

	cpService, err := csysVar.GetServiceOf(cpID, cpVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.GetConfiguration{
			Key: key,
		})
		resp := cpResp.(*csresp.GetConfiguration)
		if err == nil {
			fmt.Printf("== GET CONFIG: %v\n", resp)
			if len(resp.UnknownKey) > 0 {
				fmt.Printf("== GET CONFIG UNKNOWN KEY \n")
				respo = Response{
					Code:         201,
					Message:      "Unknown Key",
					JSONResponse: resp.UnknownKey,
				}
			} else {
				fmt.Printf("== GET CONFIG SUCCESS: %v", resp.ConfigurationKey)
				respo = Response{
					Code:         200,
					Message:      "Getting the Key-Value",
					JSONResponse: resp.ConfigurationKey,
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			respo = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    0,
			Message: "Error punto de carga no operativo",
		}
	}

	return respo
}

// Resetting ... [LISTO] reset the Central System
func ResetMachine(cpID string, ResetType string) Response {
	fmt.Printf("\n===[RESETING MACHINE (%v)]===\n", cpID)
	var respo Response = Response{
		Code:    500,
		Message: "Method Crash",
	}

	cpService, err := csysVar.GetServiceOf(cpID, cpVersion, "")

	if err == nil {
		if ResetType == "Hard" || ResetType == "Soft" {
			cpResp, err := cpService.Send(&csreq.Reset{
				/*
					Hard = Full Reset
					Soft = Return to initial status (stop any transaction in progress)
				*/
				Type: ResetType,
			})
			if err == nil {
				resp := cpResp.(*csresp.Reset)
				fmt.Printf("== RESET TYPE: %v\n", ResetType)
				fmt.Printf("== RESET RESPONSE: %v\n", resp)
				if resp.Status != "Accepted" {
					fmt.Printf("== RESET [ERROR] reject. \n")
					respo = Response{
						Code:    201,
						Message: "Rejected Reset",
					}
				} else {
					fmt.Printf("== RESET succcessful. \n")

					respo = Response{
						Code:    200,
						Message: fmt.Sprintf("Reset Complete, Status: %s", resp.Status),
					}
				}
				cs.AddLog(cpID, fmt.Sprintf("%v", resp))
			} else {
				fmt.Printf("[ERROR] %s \n", err)
				respo = Response{
					Code:    301,
					Message: err.Error(),
				}
			}
		} else {
			respo = Response{
				Code:    300,
				Message: fmt.Sprintf("El tipo de reinicio [%s] no es valido", ResetType),
			}
		}

	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    0,
			Message: "Error punto de carga no operativo",
		}
	}

	return respo
}

// ChangeConfiguration ... [LISTO]
func ChangeConfiguration(cpID string, key string, value string) Response {
	fmt.Printf("\n===[MACHINE (%v) GET CONFIG]===\n", cpID)
	var respo Response

	cpService, err := csysVar.GetServiceOf(cpID, cpVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.ChangeConfiguration{
			Key:   key,
			Value: value,
		})

		if err == nil {
			resp := cpResp.(*csresp.ChangeConfiguration)
			fmt.Printf("== ChangeConfiguration Response: %v\n", resp)

			if resp.Status != "Accepted" {
				fmt.Printf("== ChangeConfiguration Response: REJECT REQUEST\n")
				respo = Response{
					Code:    201,
					Message: "Change Reject",
				}
			} else {
				fmt.Printf("== ChangeConfiguration Response: ACCEPTED REQUEST\n")
				respo = Response{
					Code:         200,
					Message:      "Change Accepted",
					JSONResponse: resp,
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("== ChangeConfiguration Response: [ERROR] %s \n", err)
			respo = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("== ChangeConfiguration Response: [ERROR] %s \n", err)
		respo = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return respo
}

func GetMachineStatus(cpID string) Response {
	var respo Response

	if cs.IsMachineConnect(cpID) {
		fmt.Printf("\n===[MACHINE (%v) STATUS ONLINE]===", cpID)
		respo = Response{
			Code:    200,
			Message: MachineStatusMessage,
		}
	} else {
		fmt.Printf("\n===[MACHINE (%v) REQUEST OFFLINE]===", cpID)
	}
	return respo
}

// ClearCache ... [LISTO] Clear Machine
func ClearCache(cpID string) Response {
	fmt.Printf("\n===[CLEARING CACHE FOR %v]===\n", cpID)
	var respo Response

	cpService, err := csysVar.GetServiceOf(cpID, cpVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.ClearCache{})

		if err == nil {
			resp := cpResp.(*csresp.ClearCache)
			fmt.Printf("RESPUESTA CACHE: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("[ERROR] Not Accepted \n")
				respo = Response{
					Code:    201,
					Message: "Clear Cache Reject",
				}
			} else {
				respo = Response{
					Code:    200,
					Message: "Clear Cache Accepted",
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    300,
			Message: err.Error(),
		}
	}
	return respo
}

// UnlockConnector ... [FALTA]
func UnlockConnector(cpID string, connectorID int) Response {
	fmt.Printf("\n===[UNLOCK CONNECTOR #%v FOR %v]===\n", connectorID, cpID)
	var respo Response

	cpService, err := csysVar.GetServiceOf(cpID, cpVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.UnlockConnector{
			ConnectorId: connectorID,
		})

		if err == nil {
			resp := cpResp.(*csresp.UnlockConnector)
			fmt.Printf("== UC RESPONSE: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("== [ERROR] Not Accepted \n")
				respo = Response{
					Code:    201,
					Message: fmt.Sprintf("Unlocking connector [%v] rejected", connectorID),
				}
			} else {
				fmt.Printf("== [READY] Accepted \n")
				respo = Response{
					Code:    201,
					Message: fmt.Sprintf("Unlocking connector [%v] accepted", connectorID),
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    300,
			Message: err.Error(),
		}
	}
	return respo
}

// RemoteStop ... [LISTO] RemoteTransaction stop
func RemoteStop(csys cs.CentralSystem, cpID string) Response {
	var respo Response

	fmt.Printf("\nREMOTE STOP\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err == nil {
		cpResp, err := cpService.Send(&csreq.RemoteStopTransaction{
			TransactionId: 1,
		})

		if err == nil {
			resp := cpResp.(*csresp.RemoteStopTransaction)
			fmt.Printf("RESPUESTA STOP: %v\n", resp)
			if resp.Status != "Accepted" {
				fmt.Printf("[ERROR] La transaccion fue rechazada. \n")
				respo = Response{
					Code:    201,
					Message: fmt.Sprintf("Detencion remota rechazada, Status: %s", resp.Status),
				}
			} else {
				respo = Response{
					Code:    200,
					Message: fmt.Sprintf("Detencion remota aceptada, Status: %s", resp.Status),
				}
			}
			cs.AddLog(cpID, fmt.Sprintf("%v", resp))
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    300,
				Message: "Hubo un error inesperado",
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    0,
			Message: "Error punto de carga no operativo",
		}
	}

	return respo
}

// GetDiagnostic ... [LISTO] Central System can request a Charge Point for diagnostic information
func GetDiagnostic(csys cs.CentralSystem, cpID string) Response {
	fmt.Printf("\nDIAGNOSTIC START\n")
	cpVersion := ocpp.V16
	var respo Response

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err == nil {
		url := "VIRTUAL" // ? URL Required. This contains the location (directory) where the diagnostics file shall be uploaded to
		tstart := time.Now()
		tstop := time.Now().Local().Add(time.Hour * time.Duration(2))
		cpResp, err := cpService.Send(&csreq.GetDiagnostics{
			Location:      url,
			Retries:       2,
			RetryInterval: 1,
			StartTime:     &tstart,
			StopTime:      &tstop,
		})

		if err == nil {
			resp := cpResp.(*csresp.GetDiagnostics)
			fmt.Printf("RESPUESTA DIAGNOSTIC: %v\n", resp)
			if resp.FileName != "" {
				fmt.Printf("[ERROR] Error de obtencion de datos \n")
				respo = Response{
					Code:    201,
					Message: "No se pueden obtener los datos",
				}
			} else {
				respo = Response{
					Code:         200,
					Message:      "Datos obtenidos: Archivo[" + resp.FileName + "]",
					JSONResponse: resp.XMLName.Space,
				}
			}
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    300,
				Message: err.Error(),
			}
		}
	} else {
		respo = Response{
			Code:    301,
			Message: err.Error(),
		}
	}
	return respo
}

//GetCompositeSchedule ... [FALTA]
func GetCompositeSchedule(csys cs.CentralSystem, cpID string) {
	fmt.Printf("\nRESERVE NOW\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}

	cpResp, err := cpService.Send(&csreq.GetCompositeSchedule{
		ConnectorId:      1,
		Duration:         3,
		ChargingRateUnit: "W",
	})
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	resp := cpResp.(*csresp.GetCompositeSchedule)

	fmt.Printf("RESPUESTA MENSAJE: %v\n", resp)

	if resp.Status != "Accepted" {
		fmt.Printf("[ERROR] Not Accepted \n")
		return
	}

	return
}

//ReserveNow ... [FALTA]
func ReserveNow(csys cs.CentralSystem, cpID string) {
	fmt.Printf("\nRESERVE NOW\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}

	idtoken := ""
	parentidtoken := ""

	cpResp, err := cpService.Send(&csreq.ReserveNow{
		ConnectorId:   1,
		ExpiryDate:    time.Now().Add(time.Minute * 15),
		IdTag:         idtoken,
		ParentIdTag:   parentidtoken,
		ReservationId: 1,
	})
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	resp := cpResp.(*csresp.ReserveNow)

	fmt.Printf("RESPUESTA MENSAJE: %v\n", resp)

	if resp.Status != "Accepted" {
		fmt.Printf("[ERROR] Not Accepted \n")
		return
	}

	return
}

// UpdateFirmware ... [FALTA]
func UpdateFirmware(csys cs.CentralSystem, cpID string) {
	fmt.Printf("\nUpdateFirmware\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}

	cpResp, err := cpService.Send(&csreq.UpdateFirmware{
		Location:      "URL",
		Retries:       2,
		RetrieveDate:  time.Now(),
		RetryInterval: 20,
	})
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	resp := cpResp.(*csresp.UpdateFirmware)
	fmt.Printf("RESPUESTA MENSAJE: %v\n", resp)

	return
}

// CancelReservation ... [FALTA]
func CancelReservation(csys cs.CentralSystem, cpID string) {
	fmt.Printf("\nMETER VALUES\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}

	cpResp, err := cpService.Send(&csreq.CancelReservation{
		ReservationId: 1,
	})
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	resp := cpResp.(*csresp.CancelReservation)
	fmt.Printf("RESPUESTA MENSAJE: %v\n", resp)
	if resp.Status != "Accepted" {
		fmt.Printf("[ERROR] Error \n")
	}

	return
}

// ChangeAvailability ... [FALTA]
func ChangeAvailability(csys cs.CentralSystem, cpID string) {
	fmt.Printf("\nCHANGE AVAILABILITY\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	cpResp, err := cpService.Send(&csreq.ChangeAvailability{
		ConnectorId: 1,
		//Inoperative = Charge point is not available for charging. || Operative = Charge point is available for charging
		Type: "Inoperative",
	})
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	resp := cpResp.(*csresp.ChangeAvailability)
	fmt.Printf("RESPUESTA MENSAJE: %v\n", resp)
	if resp.Status != "Accepted" {
		fmt.Printf("[ERROR] Not Accepted \n")
		return
	}
}

// TriggerMessage ... [FALTA]
func TriggerMessage(csys cs.CentralSystem, cpID string) {
	fmt.Printf("\nMESSAGE START\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {

		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	cpResp, err := cpService.Send(&csreq.TriggerMessage{
		ConnectorId:      1,
		RequestedMessage: cpresp.ErrorNotChargePointResponse.Error(),
	})
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	resp := cpResp.(*csresp.TriggerMessage)
	fmt.Printf("RESPUESTA MENSAJE: %v\n", resp)
	if resp.Status != "Accepted" {
		fmt.Printf("[ERROR] Not Accepted \n")
		return
	}
}

// GetLocalList ... [LISTO]
func GetLocalList(csys cs.CentralSystem, cpID string) Response {
	fmt.Printf("\nGET LOCAL LIST VERSION\n")
	cpVersion := ocpp.V16
	var respo Response

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")

	if err == nil {
		cpResp, err := cpService.Send(&csreq.GetLocalListVersion{})
		if err == nil {
			resp := cpResp.(*csresp.GetLocalListVersion)
			fmt.Printf("RESPUESTA GET LIST: %v\n", resp)
			fmt.Printf("LISTA VERSIONES: %v\n", resp.ListVersion)
		} else {
			fmt.Printf("[ERROR] %s \n", err)
			respo = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		fmt.Printf("[ERROR] %s \n", err)
		respo = Response{
			Code:    300,
			Message: err.Error(),
		}
	}

	return respo
}

// RemoteStart ... [FALTA] Remote Start Transaction
func RemoteStart(csys cs.CentralSystem, cpID string, idTag string) {
	fmt.Printf("\n[SERVIDOR] REMOTE START\n")
	cpVersion := ocpp.V16

	cpService, err := csys.GetServiceOf(cpID, cpVersion, "")
	if err != nil {
		fmt.Printf("[ERROR] %s \n", err)
		return
	}
	tag := idTag

	cp := csreq.ChargingProfile{
		TransactionId:          1,
		ChargingProfileId:      2,
		ChargingProfileKind:    "Relative",
		ChargingProfilePurpose: "TxDefaultProfile",
		ChargingSchedule: &csreq.ChargingSchedule{
			Duration:         20,
			ChargingRateUnit: "W",
			ChargingSchedulePeriod: []*csreq.ChargingSchedulePeriodItems{
				&csreq.ChargingSchedulePeriodItems{
					StartPeriod:  time.Now().Day(),
					Limit:        30,
					NumberPhases: 5,
				},
			},
			MinChargingRate: 300,
			StartSchedule:   &time.Time{},
		},
		RecurrencyKind: "Daily",
		StackLevel:     0,
		ValidFrom:      &time.Time{},
		ValidTo:        &time.Time{},
	}

	cpResp, err := cpService.Send(&csreq.RemoteStartTransaction{
		IdTag:           tag,
		ConnectorId:     1,
		ChargingProfile: &cp,
	})
	if err != nil {
		fmt.Printf("[ERROR] %s", err)
		return
	}
	resp := cpResp.(*csresp.RemoteStartTransaction)

	fmt.Printf("[REMOTE START] Dice: %v\n", resp)

	/*if resp.Status = "Accepted" {
	fmt.Printf("[REMOTE START] Status: %v \n", resp.Status)

	} else {
		time.Sleep(10 * time.Second)

		fmt.Println("Finally kill hem...")
		//RemoteStop(csys, cpID)
		//Resetting(csys, cpID)
	}*/
}

/* METODOS DE EJECUCION */

// AsyncTask ... Async Task for start transaction after x secconds
func AsyncTask(csys cs.CentralSystem, cpIDvar string, CardID string) <-chan int32 {
	fmt.Print("[INFO] Async Task Started \n")
	r := make(chan int32)

	go func() {
		defer close(r)

		fmt.Print("[INFO] Async Task 5s \n")
		time.Sleep(time.Second * 5)
		RemoteStart(csys, cpIDvar, CardID)
		r <- rand.Int31n(100)

	}()

	return r
}

/* METODOS WEB */

//GetLocalListHTTP ...
func GetLocalListHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	w.Header().Set("Content-Type", "application/json")

	var response Response
	if r.Method == "POST" {
		if err := r.ParseForm(); err == nil {
			cpIDVar := r.FormValue("machineID")
			response = GetLocalList(csysVar, cpIDVar)
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetDiagnosticHTPP ...
func GetDiagnosticHTPP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	w.Header().Set("Content-Type", "application/json")

	var response Response
	if r.Method == "POST" {
		if err := r.ParseForm(); err == nil {
			cpIDVar := r.FormValue("machineID")
			response = GetDiagnostic(csysVar, cpIDVar)
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}

	json.NewEncoder(w).Encode(response)
}

//RemoteStopHTTP ...
func RemoteStopHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	w.Header().Set("Content-Type", "application/json")

	var response Response

	if r.Method == "POST" {
		if err := r.ParseForm(); err == nil {
			cpIDVar := r.FormValue("machineID")
			response = RemoteStop(csysVar, cpIDVar)
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}

	json.NewEncoder(w).Encode(response)
}

//ResetOCCP ...
type ResetMachineStructure struct {
	ResetType string `json:"reset_type"`
	MachineID string `json:"machineID"`
}

func ResetMachineHTTP(w http.ResponseWriter, r *http.Request) {

	setupResponse(&w, r)
	var response Response

	if r.Method == "POST" {

		bytes := BodyToJson(r)

		if err := r.ParseForm(); err == nil {

			var result ResetMachineStructure
			json.Unmarshal(bytes, &result)

			resetType := result.ResetType
			machineID := result.MachineID
			if len(resetType) < 1 {
				response = Response{
					Code:    301,
					Message: "Argument length short",
				}
			} else {
				if resetType != "" {
					if cs.IsMachineConnect(machineID) {
						response = ResetMachine(machineID, resetType)
					} else {
						response = Response{
							Code:    1,
							Message: "Maquina no conectada",
						}
					}
				} else {
					response = Response{
						Code:    301,
						Message: "Argumento no valido",
					}
				}
			}
		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}
	json.NewEncoder(w).Encode(response)
}

//ClearCacheHTTP ...
func ClearCacheHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)

	var response Response

	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {

			var result GetStatusStructure
			json.Unmarshal(bytes, &result)

			cpIDVar := result.MachineID
			if cs.IsMachineConnect(cpIDVar) {
				response = ClearCache(cpIDVar)
			} else {
				response = Response{
					Code:    0,
					Message: "Charge point is not connected",
				}
			}

		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetStatusHTPP ..
type GetStatusStructure struct {
	MachineID string `json:"machineID"`
}

func GetStatusHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var response Response

	if r.Method == "GET" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {

			var result GetStatusStructure
			json.Unmarshal(bytes, &result)

			MachineID := result.MachineID
			if cs.IsMachineConnect(MachineID) {
				response = GetMachineStatus(MachineID)
			} else {
				response = Response{
					Code:    0,
					Message: "Charge point is not connected",
				}
			}

		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}

	json.NewEncoder(w).Encode(response)
}

//ChangeConfigurationHTPP ...
type ChangeConfigurationStructure struct {
	MachineID string `json:"machineID"`
	Key       string `json:"key"`
	Value     string `json:"value"`
}

func GetConfigurationHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var response Response

	if r.Method == "GET" {
		if err := r.ParseForm(); err == nil {

			key := r.FormValue("key")
			machineID := r.FormValue("machineID")

			if cs.IsMachineConnect(machineID) {
				if len(key) < 1 {
					response = Response{
						Code:    201,
						Message: "Los valores no deben estar vacios",
					}
				} else {
					SendKey := []string{key}
					response = GetConfiguration(machineID, SendKey)
				}
			} else {
				response = Response{
					Code:    1,
					Message: "Maquina no conectada",
				}
			}

		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}
	json.NewEncoder(w).Encode(response)
}

type UnlockConnectorStructure struct {
	MachineID   string `json:"machineID"`
	ConnectorID int    `json:"connectorID"`
}

func UnlockConnectorHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var response Response

	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result UnlockConnectorStructure
			json.Unmarshal(bytes, &result)

			connectorID := result.ConnectorID
			machineID := result.MachineID

			if cs.IsMachineConnect(machineID) {
				if connectorID < 1 {
					response = Response{
						Code:    201,
						Message: "Connector ID must be > 1",
					}
				} else {
					response = UnlockConnector(machineID, connectorID)
				}
			} else {
				response = Response{
					Code:    1,
					Message: "Maquina no conectada",
				}
			}

		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}

	json.NewEncoder(w).Encode(response)
}

func ChangeConfigurationHTTP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var response Response

	if r.Method == "POST" {
		bytes := BodyToJson(r)
		if err := r.ParseForm(); err == nil {
			var result ChangeConfigurationStructure
			json.Unmarshal(bytes, &result)

			key := result.Key
			value := result.Value
			machineID := result.MachineID

			if cs.IsMachineConnect(machineID) {
				if len(key) < 1 || len(value) < 1 {
					response = Response{
						Code:    201,
						Message: "Los valores no deben estar vacios",
					}
				} else {
					response = ChangeConfiguration(machineID, key, value)
				}
			} else {
				response = Response{
					Code:    1,
					Message: "Maquina no conectada",
				}
			}

		} else {
			response = Response{
				Code:    301,
				Message: err.Error(),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}
	json.NewEncoder(w).Encode(response)
}

//GetMachinesHTPP ...
func GetMachinesHTPP(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	var response Response

	if r.Method == "GET" {
		if len(cs.ConnectedMachines) > 0 {
			response = Response{
				Code:         200,
				Message:      fmt.Sprintf("Machines connected: %v", len(cs.ConnectedMachines)),
				JSONResponse: cs.ConnectedMachines,
			}
		} else {
			response = Response{
				Code:    200,
				Message: fmt.Sprintf("No online machines"),
			}
		}
	} else {
		response = Response{
			Code:    300,
			Message: "Metodo no permitido",
		}
	}

	json.NewEncoder(w).Encode(response)
}

func BodyToJson(r *http.Request) []byte {
	bodyB, _ := ioutil.ReadAll(r.Body)
	bodyStr := string(bytes.Replace(bodyB, []byte("\r"), []byte("\r\n"), -1))
	bytes := []byte(bodyStr)

	return bytes
}

func main() {
	SetMachineStatus()
	OCPPSystem()
	HTTPServer()
}
